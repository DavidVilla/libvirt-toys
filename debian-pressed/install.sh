#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

export LIBVIRT_DEFAULT_URI=qemu:///system

name=node1
mac=00:AA:00:00:00:11

virsh destroy $name
virsh undefine $name
python -m SimpleHTTPServer 80 &

virt-install \
  --debug \
  --name=$name \
  --ram=512 \
  --disk ./$name.img,size=2,sparse=true \
  --location=http://ftp.us.debian.org/debian/dists/testing/main/installer-i386 \
  --network=network=grid,mac=00:AA:00:00:00:11 \
  --noautoconsole \
  --extra-args="\
    auto=true priority=critical vga=normal hostname=$name \
    url=http://192.168.122.1/preseed.cfg"

# http://ftp.us.debian.org/debian/dists/testing/main/installer-i386
