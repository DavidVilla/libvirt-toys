#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

export LIBVIRT_DEFAULT_URI=qemu:///system

virsh destroy node1
virt-sysprep -d node1 --enable udev-persistent-net
#virt-sysprep -d node1 --firstboot ./firstboot.sh
#virt-edit -d node1 /etc/default/puppet -e "s/=no/=yes/"
guestfish --rw -i -d node1 copy-in puppet/puppet /etc/cron.d/

for i in $(seq 2 6); do
	virt-clone --original node1 --name node$i --file ./node$i.img --mac 00:AA:00:00:00:1$i
    virt-sysprep -d node$i --enable hostname --hostname node$i
done
