#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

export LIBVIRT_DEFAULT_URI=qemu:///system

for dom in $(virsh list --all --name); do
    virsh destroy $dom
done
