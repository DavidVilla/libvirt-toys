#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

export LIBVIRT_DEFAULT_URI=qemu:///system

service puppetmaster stop
rm -rf /var/lib/puppet
hostname puppet
service puppetmaster start

for dom in $(virsh list --all --name); do
    virsh destroy $dom
    guestfish --rw -i -d $dom rm /var/lib/puppet/ssl/certs/$dom.pem
done
