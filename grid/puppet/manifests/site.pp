# -*- mode:ruby -*-

package {'ice34-services':
  ensure => present,
}

package {'psmisc':
  ensure => present,
}

file {'/etc/icegrid':
  ensure => 'directory',
  mode => 755,
  owner => root,
}

file {['/var/icegrid', '/var/icegrid/db']:
  ensure => 'directory',
  mode => 755,
  owner => root,
}

file {'/etc/icegrid/node.config':
  mode => 644,
  owner => root,
  content => "
Ice.Default.Locator=IceGrid/Locator -t:tcp -h node0 -p 4061
IceGrid.Node.Name=${hostname}
IceGrid.Node.Data=/var/icegrid/db
IceGrid.Node.Endpoints=tcp
"
}

service {'icegridnode':
  provider => 'base',
  ensure => running,
  start => '/usr/bin/icegridnode --Ice.Config=/etc/icegrid/node.config &',
  stop  => 'killall icegridnode',
  status => 'pgrep icegridnode',
  require => Package['ice34-services', 'psmisc']
}
